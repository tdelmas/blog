Suite ici : [Polémique et réponse de Trainline](https://framagit.org/tdelmas/blog/blob/master/trainline2.md)

# Étude de leurs engagements et des données personnelles qu'ils manipulent

## Leurs engagements et privacy-policy

15 novembre 2010 : https://blog.trainline.fr/2088-episode-2-devenons-des-agents-de-voyages

> Notre vision du service est déjà très claire :
>
> - **du train et uniquement du train**
> - **pas de pub**
> - des prix à l’identique
> - un billet en moins d’une minute

21 novembre 2012 : https://blog.trainline.fr/1923-lobsession-du-service-client

> Voilà ce qui nous inspire chez Capitaine Train. Voilà pourquoi nous bannirons tous les messages automatisés lorsqu’il s’agira de vous répondre. Voilà pourquoi nous serons toujours à votre écoute que ce soit sur Twitter, Facebook ou par email. Voilà pourquoi nous ferons toujours le maximum pour vous offrir un service client irréprochable. Humain, réactif et personnalisé.
> Preuve que Tony Hsieh a raison, nos clients nous le rendent bien. Parfois très bien.
> **Chaque client qui arrive sur Capitaine Train grâce au bouche-à-oreille est pour nous une victoire. Une preuve que nous avons raison de consacrer toute notre énergie à la construction de la meilleure expérience client possible, plutôt que dans des campagnes de pub.** Et quand on lit les messages que nos clients nous envoient (dans leur emails ou en public), ça nous renforce dans nos convictions.

21 février 2013 : https://blog.trainline.fr/2327-experience-capitaine-train-sans-inscription

> « Je veux juste tester, pourquoi dois-je m’inscrire ?! »
> [...]
> **Les réticences par rapport aux données personnelles sont également très fortes. Et à raison : la plupart des sites marchands n’ont aucun scrupule à utiliser votre adresse email et vos données n’importe comment.** Il est ainsi courant de se faire spammer dans tous les sens à peine l’inscription terminée sur un nouveau site. Alors on devient méfiant.

15 mars 2016 : https://blog.trainline.fr/11196-vous-avez-dit-rachat

> **Aujourd’hui, vos données personnelles sont au chaud chez Captain Train.** Dans la mesure où nous sommes rachetés, notre entreprise va être absorbée par Trainline, pour former un nouveau groupe, qui sera situé en Angleterre. Vos données passeront donc aux mains de ce groupe, groupe étranger, certes, mais groupe dans lequel nous aurons notre mot à dire. Bien que rachetés par des Anglais, les spams n’en sont pas pour autant devenus notre tasse de thé. Mais là encore, si vous jugez que nous filons un mauvais coton, dites-le nous.

Aujourd'hui

- Site web  https://trainline.fr

  - Trackers:
    - Microsoft (bing.com)
    - Google
    - Facebook
    - New Relic (https://newrelic.com)
    - Gravatar (https://automattic.com/)

  - Bandeau cookies: 

> Nous utilisons des cookies pour faire de belles statistiques, vous proposer des offres adaptées ailleurs sur le web, et améliorer notre site.
Si vous ne prenez pas de suite la fuite, nous en déduisons que vous n’avez rien contre les cookies. [Paramétrer vos cookies](https://www.trainline.fr/cookies)  


  - Cookies policy
    - 25-11-2016 (https://web.archive.org/web/20161125060844/https://www.trainline.fr/cookies)
    - 30-01-2019 (https://www.trainline.fr/cookies)
    - Changements:

Avant :

> **Les cookies ne servent pas à identifier votre personnalité, vos opinions ou votre taille de chaussures.**

Après :

> **(vide)**

Avant :

> Vous avez dû voir un bandeau d’avertissement sur notre page d’accueil **avant que nous déposions un cookie sur votre ordinateur**.

Après :

> Vous avez dû voir un bandeau d’avertissement sur notre page d’accueil.

Avant :

> Mesurer l’utilisation du site (Google Analytics)

Après : 

> Mesurer l’utilisation du site (Google Analytics **et SiteSpect**)

Avant :


> Publicité en ligne (Google et Microsoft)
> 	Quand vous faites une recherche sur Google ou sur Bing (Microsoft), certains résultats sont des pubs. Pour que vous cliquiez sur ces pubs, Google et Microsoft ont intérêt à vous afficher des pubs pertinentes. Pour cela, ils utilisent leurs propres cookies, qui servent à retenir vos préférences de navigation pour mieux vous cibler ensuite.

Après :

> Publicité en ligne (Google, Microsoft **et Facebook**)
> À l’heure actuelle, où que vous alliez sur Internet, vous verrez des publicités. **Grâce à ces cookies, notre objectif est de nous assurer que lorsque des publicités s’affichent, elles sont pertinentes et peuvent simplifier vos voyages**. Par exemple, si, à plusieurs reprises, vous avez cherché des billets pour voyager entre Paris et Lille sur notre site Internet, vous êtes susceptible de voir s’afficher une publicité qui vous proposera des offres au meilleur tarif possible. Pour cela, **nous partageons, avec des tierces parties soigneusement sélectionnées, une partie des données que collectons grâce à nos cookies publicitaires**, afin de pouvoir faire de la publicité sur leurs sites Internet. Vous êtes donc susceptible de voir s’afficher l’une de nos publicités ailleurs sur Internet.
> **Nous autorisons également certaines de ces tierces parties à déposer des cookies sur notre site Internet et notre application afin de mieux vous connaître sur la base des recherches que vous avez effectuées.** Vous trouverez leur liste ci-dessous. Ces cookies les aident à vous proposer des publicités pertinentes et à déterminer dans quelle mesure vous les avez trouvées utiles. Nous appelons ces cookies des **« cookies tiers de publicité comportementale en ligne »** (ou cookies de publicité 3P pour faire court). **Ces tierces parties sont susceptibles de partager une partie des données qu’elles collectent avec nous mais également avec d’autres tierces parties**.

Avant :

> (vide)

Après :

> Facebook
>
> **Facebook utilise des cookies et reçoit des informations lorsque vous naviguez sur le site Internet de Trainline**, dont des informations concernant votre appareil et votre activité. **Cette collecte intervient que vous ayez, ou non, un compte ou que vous soyez connecté ou non.** Elles sont conservées dans le domaine de Facebook.
> Des nombreuses informations particulièrement utiles concernant la manière dont la société utilise ces cookies sont directement fournies par Facebook ici.
> **Si vous disposez d’un compte Facebook, vous pouvez vous y opposer** ici ou modifier les paramètres du navigateur si vous n’avez pas de compte.



- Application android: https://reports.exodus-privacy.eu.org/fr/reports/54111/

  - Trackers:
    - Facebook
    - Google
    - LeanPlum (https://www.leanplum.com)
    - Tune (https://www.leanplum.com)

Demande d'accès:

Données visibles depuis l'interface:

- Informations personnelles
  - Nom
  - Prénom
  - Email

- Langue
- Devise
- Options diverses activables (newsletter, ...)

- Carte de payements

- Recherches fréquentes
  - Gare de départ
  - Gare d'arrivée

- Trajets a venir
- Trajets effectués
  - Lieu de départ
  - Lieu d'arrivé
  - Date de départ
  - Date de retours
  - Référence du billet
  - Nom des passagers
  - Numéro du train
  - Numéro des places réservés
  - Tarif et prix
  - Référence du justificatif de payement
  - Date du payement
  - Moyen de payement (4 dernier chiffres)

Dans mon compte, j'ai
- 6 recherches fréquentes visibles
- 4 voyageurs d'enregistrés
- 0 cartes banquaires d'affichées
- 6 voyages (référence unique) dont
  - 2 A/R pour 2 personnes
  - 2 A/R pour 1 personne
  - 3 trajet simple pour 1 personne

## Ma demande

> Bonjour,
> 
> Je souhaiterais une copie de L’INTÉGRALITÉ des données que vous (ainsi que les tiers sous-traitants et co-responsables) avez à mon sujet.
> 
> Donc, par exemple, mais sans se limiter à:
> - Coordonnées (passées et actuelles)
> - Historiques des payements
> - Des moyens de payement
> - Des voyages et leurs données associés bien évidement
> - Des recherches
> - Cartes de réductions (passées et actuelles)
> - Historiques des contacts au supports et commentaires liés
> - "Informations concernant la prévention de la fraude"
> - Toute information liée à mon utilisation de l'application Android (Actions effectuées, installation/désinstallations de l'application, toutes autre information, horodaté ou non), en incluant les informations récoltés par les tiers (https://reports.exodus-privacy.eu.org/fr/reports/54111/ Facebook, Google, TUNE et LeanPlum, ...)
> - Toute information liée à mon utilisation de vos sites internet (Actions effectuées, installation/désinstallations de l'application, toutes autre information, horodaté ou non), incluant les informations récoltés par les tiers (Google, Facebook, gravatar, newrelic.com, online-metrix.net, nr-data.net, bing.com, SiteSpect, ...)
> - Information de localisation (Exactes ou supposées, incluant le moment et toute autre métadonnées)
> - Informations techniques (Adresses IPs, User-Agents, Appareils, réseaux wifi...)
> - "Informations d’analyse"
> - A/B tests auxquels j'aurais pu participer ainsi que les données liés
> - Informations sur les réseaux sociaux, Y compris celles de vos prestataires tels que buffer.com
> - Connexions en cours et passées, token et heures
> - Mots de passes (tels que sauvegardés dans votre système)
> - Informations déduites (Localisations supposées, tranche d'age, loisirs, ...)
> - Liste des communications (emails, sms, ...), automatiques ou non, commerciaux ou non, que vous m'avez envoyés ainsi que toutes les données associés (incluant notablement les informations de suivis d'ouverture de mail que vous et les tiers possèdent, tel que moreannouncements.uk.com)
> - Les de diffusions auxquels vous m'avez abonnés ou blacklisté
> - Toutes les données de vos CRM me concernant (ainsi que ceux des tiers), en incluant bien évidement les commentaires manuels
> - Toutes  les informations liées à la publicité comportementale, récolté par vous ou les tiers
> 
> - Y compris les informations qui sont ne sont plus que dans vos sauvegardes (A l’exception des copie de sauvegardes systématiquement détruites au bout de 30 jours maximum qui ne sont crées que dans l'unique but de prévenir à une perte de données accidentelle)
> 
> Bref, TOUT, par vous et les tiers.
> 
> Je souhaiterais également la liste complète des tiers (Exemple, utilisés pour le payement,la prévention de la fraude, l'achat de billet, la gestion des réseaux sociaux, présent sur vos sites ou vos appli, pour la publicité comportementale, les emails, sms et notifications push...) auxquels vous avez transmis des données me concernant (Ainsi que la liste des données auxquels vous leur avez donné accès).

## Leur réponse

```json
{
  "account": {
    "first_name": Prénom,
    "last_name": Nom,
    "email": Email,
    "signed_up_at": Date d'inscription,
    "facebook_id": Identifiant Facebook,
    "facebook_infos": Informations obtenues depuis facebook,
    "google_id": Identifiant Google,
    "google_data": Informations obtenues depuis Google,
    "twitter_username": Identifiant Twitter,
    "invited_users": [ Liste des utilisateurs parainés ],
    "sessions": [ Liste des sessions en cours
      {
        "ip": IP de la session,
        "started_at": Date de début de la séssion
      }
    ],
    "addresses": [ Liste des adresses connues (???)
      {
        "street": Rue,
        "locality": Ville,
        "region": Région,
        "postcode": Code postal,
        "country": Pays,
        "phone": Téléphone
      }
    ],
    "identification_documents": [ Liste des documents d'identification
    ],
    "notes": [ ???

    ],
    "payment_cards": [ Liste des cartes de payements connus
		6 entrées pour 4 cartes
      {
        "label": Ex.: "Visa XXXX",
        "last_digits": 4 derniers chiffres,
        "holder": Nom de détenteur,
        "country": Code pays,
        "shared": ???,
        "added_at": Date d'ajout/d'utilisation,
        "used_as_saved_count": ???
      }
    ]
  },
  "passengers": [ Liste des passagers du comptes
    {
      "id": Identifiant de passager Trainline,
      "first_name": Prénom,
      "last_name": Nom,
      "gender": Genre,
      "birthdate": Date de naissance,
      "email": Email,
      "phone": Téléphone,
      "kind": Type (humain ou animal),
      "cards": [ Cartes de réductions
        {
          "reference": Nom technique,
          "number": Identifiant de la carte
        }
      ]
    }
  ],
  "searches": [ Recherche: 137 éléments
    {
      "searched_at": Date de la recherche,
      "departure": {
        "public_id": Identifiant du lieu de départ,
        "latitude": Latitude du lieu de départ,
        "longitude": Longitude du lieu de départ,
        "time_zone": Timezone du lieu de départ,
        "country": Pays du lieu de départ,
        "slug": nom du lieu de départ,
        "name": nom du lieu de départ
      },
      "arrival": {
        "public_id": Identifiant du lieu d'arrivé,
        "latitude": Latitude du lieu de d'arrivé,
        "longitude": Longitude du lieu de d'arrivé,
        "time_zone": Timezone du lieu de d'arrivé,
        "country": Pays du lieu de d'arrivé,
        "slug": nom du lieu de d'arrivé,
        "name": nom du lieu de d'arrivé
      },
      "departure_date": Date du départ,
      "return_date": Date du retour,
      "passengers": [ Nom complet des passages ou "(forgotten) (forgotten)" ]
    },
  ],
  "payments": [
    {
      "state": "Success", 9 éléments
      "started_at": Date du payement,
      "quick_payment": null,
      "payment_mean": {
        "type": "payment_card",
        "last_digits": 4 dernier chiffres de la carte,
        "holder": Nom du détenteur de la carte,
        "country": Pays
      },
      "ip": IP,
      "amount": {
        "cents": Montant du payement,
        "currency": Devise
      },
      "charged_amount": {
        "cents": Montant du payement,
        "currency": Devise
      }
    },
    {
      "state": "Error", 7 éléments
      "started_at": Date,
      "quick_payment": null,
      "payment_mean": null,
      "ip": IP,
      "amount": {
        "cents": Montant du payement,
        "currency": Devise
      },
      "charged_amount": {
        "cents": Montant du payement,
        "currency": Devise
      }
    },
  ],
  "subscriptions": [ Cartes d'abonements achetés? 6 éléments
    {
      "card_type": Type de carte,
      "code": Code court SNCF?,
      "validity": null,
      "effective_date": Date de début de validité,
      "origin": null,
      "via": null,
      "destination": null,
      "traveler": {
        "uuid": ID SNCF?,
        "gender": Genre,
        "first_name": Prénom,
        "last_name": Nom,
        "phone": Téléphone,
        "email": Email,
        "birthdate": Date de naissance,
        "sncf_first_name": Nom pour la SNCF,
        "sncf_last_name": Prénom pour la SNCF,
        "sncf_email": null,
        "sncf_loyalty_card_number": null,
        "sncf_reference_card_number": null
      },
      "price": {
        "cents": Prix,
        "currency": Device
      },
      "card_number": Numéro de la carte
    },
  ],
  "pnrs": [
    {
      "status": "booking_cancelled" (4 éléments) ou "emitted" (6 éléments) ou "booking_expired" (5 éléments),
      "booked_at": Date de la réservation,
      "emitted_at": null,
      "public_token": ID SNCF de la réservation?,
      "code": null,
      "identification_document": {
        "type": null,
        "holder": null
      },
      "legs": [ Décomposition du voyage (par train et passager)
        {
          "passenger_id": Identifiant de passager Trainline,
          "departure_date": Date de départ,
          "arrival_date": Date d'arrivée,
		  
		  "departure": {
			"public_id": Identifiant du lieu de départ,
			"latitude": Latitude du lieu de départ,
			"longitude": Longitude du lieu de départ,
			"time_zone": Timezone du lieu de départ,
			"country": Pays du lieu de départ,
			"slug": nom du lieu de départ,
			"name": nom du lieu de départ
		  },
		  "arrival": {
			"public_id": Identifiant du lieu d'arrivé,
			"latitude": Latitude du lieu de d'arrivé,
			"longitude": Longitude du lieu de d'arrivé,
			"time_zone": Timezone du lieu de d'arrivé,
			"country": Pays du lieu de d'arrivé,
			"slug": nom du lieu de d'arrivé,
			"name": nom du lieu de d'arrivé
		  },
	  
          "travel_class": Classe (1ère, 2nd),
          "carrier": Compagnie de transport,
          "train": {
            "type": Type de train,
            "number": Numméro du train
          },
          "ticket_status": null,
          "co2_emission": Estimmation d'émissions de CO2,
          "conditions": { Détails des conditions contractuelles }
        }
      ],
      "travelers": [
        {
          "uuid": ???,
          "gender": Genre,
          "first_name": Prénom,
          "last_name": Nom,
          "phone": Téléphone,
          "email": Email,
          "birthdate": Date de naissance,
          "sncf_first_name": Nom pour la SNCF,
          "sncf_last_name": Prénom pour la SNCF,
          "sncf_email": Email pour la SNCF,
          "sncf_loyalty_card_number": Numéro de carte de fidélité SNCF,
          "sncf_reference_card_number": Numéro de carte de transport SNCF
        }
      ],
      "cancelled": Si le voyage a été annulé,
      "price": {
        "cents": Prix,
        "currency": Devise
      }
    }
  ]
}

```