# Début de la polémique:

https://twitter.com/dbertho/status/1100815235479465987
David Bertho @dbertho
6:49 PM · 27 févr. 2019 · Twitter Web Client
> Hi, @trainline_eu
> 
> It seems that your app is sending data to Facebook. It includes from/to cities, currency, number of passengers, a unique personnal ID assigned by Google or Apple, ...
> Did I miss the text telling me my personnal data will be shared with advertising partners ?

# Réponses de Trainline

## Résumé

Hi there, we work with carefully selected partners for the purposes of analytics: to understand how you use our website and app, to help us improve it and to make it relevant to your interests.
We tell you about this in our privacy policy which we show you at the point when you create an account with us and when you buy a ticket from us. More details are in the policy here: thetrainline.com/terms/privacy

We're not providing Facebook with your personal or travel data for their own uses. We are using an analytics tool, created by Facebook, to better understand how our customers are using our app.
It lets us analyse usage patterns, and make the user experience better and more relevant based on what we find out.

Nous ne transmettons pas vos informations personnelles ou de voyage à Facebook pour qu’ils les utilisent pour leurs propres usages.
Nous utilisons un outil d'analyse, créé par Facebook, qui nous permet de mieux comprendre comment nos clients utilisent notre app. Cela nous permet d’analyser les utilisations faites et de rendre l'expérience utilisateur meilleure et plus pertinente.

We need to use info about the searches and bookings you make for our products to work better and to improve the services we provide to you, so we do have a legitimate interest for analytics. You can get more detailed info in our privacy policy - https://www.thetrainline.com/terms/privacy#personal-data

Même si les données passent par l'outil d'analyse de Facebook, elles ne sont pas partagées avec Facebook – Trainline contrôle ces données. Le respect des données et la sécurité de nos clients est primordiale et la protection des données est au coeur de notre activité.

# Détails

https://twitter.com/trainline_eu/status/1101112530284482560
Trainline Europe @trainline_eu
En réponse à @dbertho
2:30 PM · 28 févr. 2019 · Buffer
> Hi there, we work with carefully selected partners for the purposes of analytics: to understand how you use our website and app, to help us improve it and to make it relevant to your interests. 1/2

https://twitter.com/trainline_eu/status/1101112559141294080
Trainline Europe @trainline_eu
En réponse à @trainline_eu et  @dbertho
2:31 PM · 28 févr. 2019 · Buffer
> We tell you about this in our privacy policy which we show you at the point when you create an account with us and when you buy a ticket from us. More details are in the policy here: thetrainline.com/terms/privacy 2/2

https://twitter.com/trainline_eu/status/1101371549200904193
Trainline Europe @trainline_eu
En réponse à @dbertho
7:40 AM · 1 mars 2019 · Twitter Web Client
> We appreciate that you’ve shared this feedback with us and we have shared it with the appropriate department. If you have further questions, please feel free to send a DM and we can connect you with that department to address any concerns you may have.

https://twitter.com/trainline_eu/status/1101446623815704578
Trainline Europe @trainline_eu
En réponse à @trainline_eu et  @dbertho
12:38 PM · 1 mars 2019 · Twitter Web Client
> Our data team has shared some extra info for you. We're not providing Facebook with your personal or travel data for their own uses. We are using an analytics tool, created by Facebook, to better understand how our customers are using our app. (1/2)

https://twitter.com/trainline_eu/status/1101447731397214209
Trainline Europe @trainline_eu
En réponse à @dbertho
12:42 PM · 1 mars 2019 · Twitter Web Client
> Our data team has shared some extra info for you. We're not providing Facebook with your personal or travel data for their own uses. We are using an analytics tool, created by Facebook, to better understand how our customers are using our app. (1/2)

https://twitter.com/trainline_eu/status/1101446695907454978
Trainline Europe @trainline_eu
En réponse à @trainline_eu et  @dbertho
12:43 PM · 1 mars 2019 · Twitter Web Client
> It lets us analyse usage patterns, and make the user experience better and more relevant based on what we find out. (2/2)

https://twitter.com/trainline_eu/status/1101447758056165378
Trainline Europe @trainline_eu
En réponse à @trainline_eu et  @dbertho
12:43 PM · 1 mars 2019 · Twitter Web Client
> It lets us analyse usage patterns, and make the user experience better and more relevant based on what we find out. (2/2)

https://twitter.com/trainline_fr/status/1101460656929341440
Trainline France @trainline_fr
En réponse à @gchampeau
1:34 PM · 1 mars 2019 · Buffer
> Bonjour Guillaume, nous ne transmettons pas vos informations personnelles ou de voyage à Facebook pour qu’ils les utilisent pour leurs propres usages. 1/2


https://twitter.com/trainline_fr/status/1101460736994406400
Trainline France @trainline_fr En réponse à @gchampeau
1:34 PM · 1 mars 2019 · Buffer
> Nous utilisons un outil d'analyse, créé par Facebook, qui nous permet de mieux comprendre comment nos clients utilisent notre app. Cela nous permet d’analyser les utilisations faites et de rendre l'expérience utilisateur meilleure et plus pertinente. 2/2

https://twitter.com/trainline_eu/status/1101461316471009280
Trainline Europe @trainline_eu
En réponse à @joachimesque
1:36 PM · 1 mars 2019 · Buffer
> Bonjour Joachim, nous ne transmettons pas vos informations personnelles ou de voyage à Facebook pour qu’ils les utilisent pour leurs propres usages. 1/2

Trainline Europe
@trainline_eu
En réponse à @trainline_eu et  @joachimesque
1:37 PM · 1 mars 2019 · Buffer
> Nous utilisons un outil d'analyse, créé par Facebook, qui nous permet de mieux comprendre comment nos clients utilisent notre app. Cela nous permet d’analyser les utilisations faites et de rendre l'expérience utilisateur meilleure et plus pertinente. 2/2

https://twitter.com/trainline_eu/status/1101463952742117376
Trainline Europe @trainline_eu
En réponse à @aeris22 @FabianRODES et 2 autres
1:47 PM · 1 mars 2019 · Twitter Web Client
> Hi aeris, we need to use info about the searches and bookings you make for our products to work better and to improve the services we provide to you, so we do have a legitimate interest for analytics. You can get more detailed info in our privacy policy - https://www.thetrainline.com/terms/privacy#personal-data

https://twitter.com/trainline_fr/status/1101917184601792513
Trainline France @trainline_fr
En réponse à @latrive
7:48 PM · 2 mars 2019 · Buffer
> Bonsoir, Même si les données passent par l'outil d'analyse de Facebook, elles ne sont pas partagées avec Facebook – Trainline contrôle ces données. Le respect des données et la sécurité de nos clients est primordiale et la protection des données est au coeur de notre activité.

# Thread principal - https://web.archive.org/web/20190303190304/https:/twitter.com/trainline_eu/status/1101446695907454978

David Bertho @dbertho
27 févr.
> Hi, @trainline_eu
> It seems that your app is sending data to Facebook. It includes from/to cities, currency, number of passengers, a unique personnal ID assigned by Google or Apple, ...
> Did I miss the text telling me my personnal data will be shared with advertising partners ?

Trainline Europe @trainline_eu
28 févr.
> Hi there, we work with carefully selected partners for the purposes of analytics: to understand how you use our website and app, to help us improve it and to make it relevant to your interests. 1/2

Trainline Europe @trainline_eu
28 févr.
> We tell you about this in our privacy policy which we show you at the point when you create an account with us and when you buy a ticket from us. More details are in the policy here: (link: https://www.thetrainline.com/terms/privacy) thetrainline.com/terms/privacy 2/2

David Bertho @dbertho
28 févr.
> Thanks, I guess I missed it :) 
> I'm being picky but you say that you "don't routinely share" info about searches and bookings. But from what I noticed, you do. You systematically share this data.

David Bertho @dbertho
28 févr.
> And you explain that you only work with service providers that prove they have the same high data protection standards as yourself. Would you say that you have the same standards of data protection as Facebook ? They proved multiple times that their standards are rather weak.

Trainline Europe @trainline_eu
1 mars
> We appreciate that you’ve shared this feedback with us and we have shared it with the appropriate department. If you have further questions, please feel free to send a DM and we can connect you with that department to address any concerns you may have.

Trainline Europe @trainline_eu
1 mars
> Our data team has shared some extra info for you. We're not providing Facebook with your personal or travel data for their own uses. We are using an analytics tool, created by Facebook, to better understand how our customers are using our app. (1/2)

Trainline Europe @trainline_eu
> It lets us analyse usage patterns, and make the user experience better and more relevant based on what we find out. (2/2)

David Bertho @dbertho
1 mars
> Other people much more qualified than me on the legal aspect of this are already reacting. But as a customer, it feels like a breach of trust. Facebook have proved they cannot be trusted with data, even analytics.

